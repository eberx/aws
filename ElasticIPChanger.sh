#!/bin/bash
# I'm new to bash scripting!!!
#
# This script is for change Elastic IP of AWS EC2 instance.
# 
#
# ATTENTION
# * Please do not use this code on Production server.
# * change one parameter at line 27 eu-central-1 to YOUR REGION
# * This script must run on server itself
##

red='\033[0;31m'
green='\033[0;32m'
yellow='\033[1;33m'
NC='\033[0m'

#releasedEIP="/usr/local/etc/releasedEIP.log"

myPublicIP=$(ec2metadata --public-ipv4)
echo -e "${green}myPublicIP $myPublicIP${NC}"
myInstanceId=$(ec2metadata --instance-id)
echo -e "${green}instanceId $instanceId${NC}"
myLocalIP=$(ec2metadata --local-ipv4)
echo -e "${green}myLocalIP $myLocalIP${NC}"

myAllocationId=$(aws ec2 describe-addresses --region eu-central-1 | grep -w -A 1 "$myPublicIP" | xargs | awk -F: '{print $NF}' | sed 's/ //g' | sed 's/,//g')
echo -e "${green}myAllocationId $myAllocationId${NC}"

# find myself from EIP return Number
findMe=$(aws ec2 describe-addresses --region eu-central-1 | grep -w "$myPublicIP" | wc -l)
echo -e "findMe $findMe"

# Count EIP
countEIP=$(aws ec2 describe-addresses --region eu-central-1 | grep -w PublicIp | wc -l)

# Count Unused IP address
# countUnusedIP=$(aws ec2 describe-addresses --query 'Addresses[?InstanceId==null]' | grep AllocationId |  awk -F: '{print $NF}' | sed 's/"//g' | sed 's/ //g' | head -1)

echo -e "${green}countEIP $countEIP${NC}"

executeEIPComplete () {

                        AssociationId=$(aws ec2 describe-addresses --allocation-ids $1 | grep AssociationId | awk -F: '{print $NF}' | sed 's/,//g' | sed 's/ //g' | sed 's/"//g' | head -1)
                        echo -e "${green}Disassociating EIP $myPublicIP${NC}"
                        aws ec2 disassociate-address --association-id $AssociationId
                        echo -e "${green}Done${NC}"
                        
                        echo -e "${green}Releasing EIP $myPublicIP${NC}"
                        aws ec2 release-address --allocation-id $1
                        echo -e "${green}Done${NC}"      

                        echo -e "${green}Allocating new EIP${NC}"
                        allocatedId=$(aws ec2 allocate-address --domain vpc --region eu-central-1 | grep AllocationId |  awk -F: '{print $NF}' | sed 's/,//g' | sed 's/ //g' | sed 's/"//g')
                        echo -e "${green}Done$allocatedId${NC}"
                        
                        echo -e "${green}Associating EIP${NC}"
                        aws ec2 associate-address --instance-id $2 --allocation-id $allocatedId --private-ip-address $3
                        echo -e "${green}Associated${NC}"
}
executeEIP () {
                        echo -e "${green}Allocating new EIP $allocatedId${NC}"
                        allocatedId=$(aws ec2 allocate-address --domain vpc --region eu-central-1 | grep AllocationId |  awk -F: '{print $NF}' | sed 's/,//g' | sed 's/ //g' | sed 's/"//g')
                        echo -e "${green}Done${NC}"

                        echo -e "${green}Associating EIP${NC}"
                        aws ec2 associate-address --instance-id $1 --allocation-id $allocatedId --private-ip-address $3
                        echo -e "${green}Done${NC}"

}


if [ $findMe -eq 1 ]
then
        executeEIPComplete $myAllocationId $myInstanceId $myLocalIP
else

        if [ $countEIP -eq 5 ]
        then
                echo -e "${red}Fatal Error: I'm not in EIP list${red}"
        elif [ $countEIP -lt 5 ]
        then
                echo -e "${yellow}Notice: My IP not in EIP${yellow}"
                executeEIP $instanceId
        else
                echo -e "${red}Fatal Error: All 5 EIP is associated${red}"
        fi
fi
